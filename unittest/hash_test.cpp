/* Copyright (c) 2021 OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by killtimer on 2023/12/11.
//

#include <vector>
#include "gtest/gtest.h"
#include "common/hash.h"

TEST(hash, hash_calc_empty)
{
  uint8_t result[16], expected[16] = {
    0xd4, 0x1d, 0x8c, 0xd9,
    0x8f, 0x00, 0xb2, 0x04,
    0xe9, 0x80, 0x09, 0x98,
    0xec, 0xf8, 0x42, 0x7e,
  };
  hash_calc(nullptr, 0, result);
  ASSERT_EQ(memcmp(result, expected, 16), 0);
}

TEST(hash, hash_calc_1)
{
  uint8_t result[16], expected[16] = {
    0x2d, 0xbd, 0x72, 0xb1,
    0x82, 0x01, 0xc1, 0x4a,
    0x6a, 0xf1, 0x02, 0xe1,
    0x07, 0x2c, 0xa5, 0x3a,
  };
  uint8_t data[] = {
    0xc2, 0x8b, 0x65, 0x53, 0x0d,
    0xbd, 0x4d, 0xa3, 0x94, 0x86,
    0x12, 0x58, 0xdb, 0xf6, 0xd3, 0xa7,
  };
  hash_calc(data, sizeof (data), result);
  ASSERT_EQ(memcmp(result, expected, 16), 0);
}

TEST(hash, hash_calc_2)
{
  uint8_t result[16], expected[16] = {
    0xe9, 0x37, 0xaa, 0xdc,
    0xb9, 0x1a, 0x16, 0x16,
    0xd7, 0x32, 0x67, 0x69,
    0x91, 0x34, 0x19, 0x3d,
  };
  uint8_t data[] = {
    0xd6, 0xdb, 0xd1, 0x30,
    0xd7, 0xb0, 0x9d, 0xab,
    0x58, 0xa5, 0xe8, 0xcc,
    0x29, 0x39, 0xb3, 0xfa,
    0x20, 0xb3, 0x1e, 0x54,
    0x39, 0x15, 0x20, 0x25,
    0x90, 0x12, 0x9c, 0xa2,
    0x49, 0xa2, 0xff,
  };
  hash_calc(data, sizeof (data), result);
  ASSERT_EQ(memcmp(result, expected, 16), 0);
}

int main(int argc, char **argv)
{
  // 分析gtest程序的命令行参数
  testing::InitGoogleTest(&argc, argv);

  // 调用RUN_ALL_TESTS()运行所有测试用例
  // main函数返回RUN_ALL_TESTS()的运行结果
  return RUN_ALL_TESTS();
}
