/* Copyright (c) 2021 OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by Killtimer on 2023/11/12.
//

#include "sql/stmt/open_column_stmt.h"
#include "common/log/log.h"
#include "storage/db/db.h"
#include "storage/table/table.h"

RC OpenColumnStmt::create(Db *db, const OpenColumnSqlNode &open_column, Stmt *&stmt)
{
  Table *table = db->find_table(open_column.relation_name.c_str());
  if (table == nullptr) {
    return RC::SCHEMA_TABLE_NOT_EXIST;
  }

  FieldMeta *field_meta = table->table_meta().field_mut(open_column.column_name.c_str());
  if (nullptr == field_meta) {
    LOG_WARN("no such field in table. db=%s, table=%s, field name=%s", 
             db->name(), open_column.column_name.c_str(), open_column.column_name.c_str());
    return RC::SCHEMA_FIELD_NOT_EXIST;
  }
  stmt = new OpenColumnStmt(table, field_meta, open_column.password);
  return RC::SUCCESS;
}
