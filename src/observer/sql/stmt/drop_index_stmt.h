/* Copyright (c) 2021 OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by Killtimer on 2023/10/31.
//

#pragma once

#include <string>

#include "sql/stmt/stmt.h"

struct DropIndexSqlNode;
class Table;
class Index;

/**
 * @brief 删除索引的语句
 * @ingroup Statement
 */
class DropIndexStmt : public Stmt
{
public:
  DropIndexStmt(Table *table, Index *index)
        : table_(table),
          index_(index)
  {}

  virtual ~DropIndexStmt() = default;

  StmtType type() const override { return StmtType::DROP_INDEX; }

  Table *table() const { return table_; }
  Index *index() const { return index_; }

public:
  static RC create(Db *db, const DropIndexSqlNode &create_index, Stmt *&stmt);

private:
  Table *table_ = nullptr;
  Index *index_ = nullptr;
};
