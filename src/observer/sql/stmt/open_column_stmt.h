/* Copyright (c) 2021 OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by Killtimer on 2023/11/12.
//

#pragma once

#include <string>
#include <vector>

#include "sql/stmt/stmt.h"

class Db;
class Table;
class FieldMeta;

/**
 * @brief 打开加密字段的语句
 * @ingroup Statement
 * @details 虽然解析成了stmt，但是与原始的SQL解析后的数据也差不多
 */
class OpenColumnStmt : public Stmt
{
public:
  OpenColumnStmt(Table *table, FieldMeta *field_meta, const std::string &password)
        : table_(table), field_meta_(field_meta), password_(password)
  {}
  virtual ~OpenColumnStmt() = default;

  StmtType type() const override { return StmtType::OPEN_COLUMN; }

  Table *table() const { return table_; }

  const FieldMeta *field_meta() const { return field_meta_; }
  FieldMeta *field_meta() { return field_meta_; }

  const std::string &password() const { return password_; }

  static RC create(Db *db, const OpenColumnSqlNode &open_column, Stmt *&stmt);

private:
  Table *table_;
  FieldMeta *field_meta_;
  std::string password_;
};