/* Copyright (c) 2021 OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by Killtimer on 2023/11/05.
//

#pragma once

#include <cstdint>
#include <algorithm>
#include "common/hash.h"
#include "common/cipher.h"

class CipherState {
public:
  CipherState() = default;
  explicit CipherState(const std::string &key) : state(State::CipherKey)
  { data.cipher = Cipher(key); }

  explicit CipherState(const uint8_t hash[16]) : state(State::CipherHash)
  { std::copy_n(hash, 16, data.hash); }

  static CipherState from_password(const std::string &password)
  { if (password.empty()) { return {}; } else { return CipherState(password); } }

  bool can_use() const
  { return state != State::CipherHash; }

  bool try_use_cipher(Cipher &cipher) const
  {
    if (state == State::CipherKey) {
      cipher = data.cipher;
      return true;
    }
    return false;
  }

  bool try_update_cipher(const std::string &key);

  bool encrypted() const
  { return state != State::NoCipher; }

  bool export_hash(uint8_t hash[16]) const {
    if (state == State::CipherKey) {
      data.cipher.export_hash(hash);
      return true;
    } else if (state == State::CipherHash) {
      std::copy_n(data.hash, 16, hash);
      return true;
    } else {
      return false;
    }
  }

private:
  enum class State {
    NoCipher, // 未加密
    CipherHash, // 已加密，但只知道密钥的哈希值
    CipherKey // 已加密，并可以获取密钥
  };
  State state = State::NoCipher;
  union {
    uint8_t hash[16];
    Cipher cipher;
  } data = {0};
};
