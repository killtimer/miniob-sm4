/* Copyright (c) 2021 OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by Wangyunlai on 2023/03/07.
//

#pragma once

#include <stdint.h>
#include "common/types.h"
#include "common/rand.h"
#include "storage/common/cipher_state.h"

using TrxID = int32_t;

static constexpr int BP_INVALID_PAGE_NUM = -1;

static constexpr PageNum BP_HEADER_PAGE   = 0;

static constexpr const int BP_PAGE_SIZE = (1 << 13);
static constexpr const int BP_PAGE_DATA_SIZE = (BP_PAGE_SIZE - sizeof(PageNum) - sizeof(LSN) - 16);

/**
 * @brief 表示一个页面，可能放在内存或磁盘上
 * @ingroup BufferPool
 */
struct Page
{
  uint8_t nonce[16];
  PageNum page_num;
  LSN     lsn;
  char data[BP_PAGE_DATA_SIZE];

  bool encrypt_if_needed(const CipherState &cipher_state, Page &output) {
    if (cipher_state.encrypted()) {
      Cipher cipher;
      if (cipher_state.try_use_cipher(cipher)) {
        SecRand::randbytes(output.nonce, 16);
        cipher.encrypt_ctr(output.nonce, (uint8_t *)this + 16, BP_PAGE_SIZE - 16, (uint8_t *)&output + 16);
        return true;
      } else {
        return false;
      }
    } else {
      output = *this;
      return true;
    }
  }

  bool decrypt_if_needed(const CipherState &cipher_state, Page &output) {
    if (cipher_state.encrypted()) {
      Cipher cipher;
      if (cipher_state.try_use_cipher(cipher)) {
        cipher.decrypt_ctr(nonce, (uint8_t *)this + 16, BP_PAGE_SIZE - 16, (uint8_t *)&output + 16);
        return true;
      } else {
        return false;
      }
    } else {
      output = *this;
      return true;
    }
  }
};
