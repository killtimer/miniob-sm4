/* Copyright (c) 2021 OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created on 2023/11/05.
//

#pragma once

#include <cstdint>

/**
 * @brief 求出给定数据的哈希值
 * @details 目前使用MD5算法
 * 
 * @param data    输入数据
 * @param len     输入数据长度
 * @param result  输出结果
*/
void hash_calc(const uint8_t *data, std::size_t len, uint8_t result[16]);
